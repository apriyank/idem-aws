import copy
import json
import time
import uuid

import pytest


@pytest.mark.asyncio
async def test_policy(hub, ctx):
    policy_temp_name = "idem-test-policy-" + str(int(time.time()))
    policy_document = {
        "Statement": [
            {"Action": ["ec2:DescribeTags"], "Effect": "Allow", "Resource": "*"}
        ],
        "Version": "2012-10-17",
    }

    description = "Idem IAM policy integration test"
    tags = [{"Key": "Name", "Value": policy_temp_name}]
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=policy_temp_name,
        policy_document=policy_document,
        description=description,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    # For policy arn is the identifier used to find an existing object
    resource_id = resource.get("resource_id")
    assert resource_id, "Expecting resource id"
    assert policy_temp_name == resource.get("name")
    assert description == resource.get("description")
    assert hub.tool.utils.verify_lists_identical(tags, resource.get("tags"))
    assert json.dumps(policy_document, sort_keys=True) == resource.get(
        "policy_document"
    )

    # Test updating policy document and adding tags
    tags.append(
        {
            "Key": f"iam-policy-key-{str(uuid.uuid4())}",
            "Value": f"iam-policy-value-{str(uuid.uuid4())}",
        }
    )
    policy_document = '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=policy_temp_name,
        resource_id=resource_id,
        policy_document=policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.utils.verify_lists_identical(tags, resource.get("tags"))

    assert (
        ret.get("old_state")["default_version_id"]
        != ret.get("new_state")["default_version_id"]
    )

    # Testing with test flag - start
    # Create policy with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.policy.present(
        test_ctx,
        name=policy_temp_name + "-test",
        policy_document=policy_document,
        description="test-ctx-description",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert "Would create" in str(ret["comment"]), ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("description") == "test-ctx-description"

    # Update tags and change policy on existing policy with test flag
    test_tags = [{"Key": "Test-Name", "Value": policy_temp_name + "test"}]
    test_policy_document = {
        "Statement": [
            {"Action": ["ec2:DeleteVpc"], "Effect": "Allow", "Resource": "*"}
        ],
        "Version": "2012-10-17",
    }
    ret = await hub.states.aws.iam.policy.present(
        test_ctx,
        name=policy_temp_name,
        resource_id=resource_id,
        policy_document=test_policy_document,
        description=description,
        tags=test_tags,
    )
    assert ret["result"], ret["comment"]
    assert "Would update" in str(ret["comment"]), ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("tags") == test_tags
    assert resource.get("policy_document") == json.dumps(test_policy_document)

    # Delete policy with test flag
    ret = await hub.states.aws.iam.policy.absent(
        test_ctx,
        name=policy_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert "Would delete" in str(ret["comment"]), ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    # Testing with test flag - end

    # Test deleting tags
    tags = [tags[0]]
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=policy_temp_name,
        resource_id=resource_id,
        policy_document=policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.utils.verify_lists_identical(tags, resource.get("tags"))

    # Verify no change to policy
    assert (
        ret.get("old_state")["default_version_id"]
        == ret.get("new_state")["default_version_id"]
    )

    # Describe IAM policy
    describe_ret = await hub.states.aws.iam.policy.describe(ctx)
    assert resource_id in describe_ret

    resource = describe_ret[resource_id].get("aws.iam.policy.present")

    for describe_param in resource:
        # assert sls name property contains the policy name literal
        if "name" == describe_param:
            assert policy_temp_name == describe_param

        if "tags" in describe_param:
            assert hub.tool.utils.verify_lists_identical(
                tags, describe_param.get("tags")
            )

    # Delete IAM policy
    ret = await hub.states.aws.iam.policy.absent(
        ctx, name=policy_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete IAM policy again
    ret = await hub.states.aws.iam.policy.absent(
        ctx, name=policy_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
