import pytest
import yaml
from pytest_idem.runner import idem_cli
from pytest_idem.runner import run_yaml_block

# Parametrization options for running each test with --test first and then without --test
PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

# Search for an ami that exists in docker: https://docs.localstack.cloud/aws/elastic-compute-cloud/
# For local tests, create a tag like this on an ubuntu:focal container:
#     $ docker tag ubuntu:focal localstack-ec2/idem-test-ami:ami-000001
IMAGE_DOCKER = """
test_ami:
  aws.ec2.ami.search:
    - most_recent: False
    - filters:
      - name: name
        values:
          - idem-test-ami
"""

IMAGE_ANY = """
test_ami:
  aws.ec2.ami.search:
    - most_recent: False
    - filters:
      - name: image-type
        values:
          - machine
      - name: state
        values:
          - available
      - name: hypervisor
        values:
          - xen
      - name: architecture
        values:
          - x86_64
      - name: virtualization-type
        values:
          - paravirtual
"""

INSTANCE_TYPE = """
nano:
  aws.ec2.instance_type.search:
    - filters:
      - name: instance-type
        values:
          - '*.nano'
      - name: hypervisor
        values:
          - xen
      - name: processor-info.supported-architecture
        values:
          - x86_64
"""

SEARCH_STATE = """
verify:
  aws.ec2.instance.search:
    - resource_id: ${aws.ec2.instance:test_resource:resource_id}
"""

PRESENT_STATE = f"""
{INSTANCE_TYPE}
test_resource:
  aws.ec2.instance.present:
    - image_id: ${{aws.ec2.ami:test_ami:resource_id}}
    - instance_type: ${{aws.ec2.instance_type:nano:resource_id}}
{SEARCH_STATE}
"""

ABSENT_STATE = f"""
test_resource:
  aws.ec2.instance.absent
{SEARCH_STATE}
"""


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="image")
def test_image(acct_data, __test):
    """
    Verify that we can get an image to do the rest of the tests
    """
    global ABSENT_STATE
    global PRESENT_STATE

    # Try to get the image created for this test by docker
    ret = run_yaml_block(IMAGE_DOCKER, acct_data=acct_data, test=__test)
    image_ret = ret["aws.ec2.ami_|-test_ami_|-test_ami_|-search"]

    if image_ret["result"]:
        # Image from docker exists, use that for localstack
        PRESENT_STATE += IMAGE_DOCKER
        ABSENT_STATE += IMAGE_DOCKER
    else:
        # No image from docker exists, use any image we can find for real aws
        ret = run_yaml_block(IMAGE_ANY, acct_data=acct_data)
        image_ret = ret["aws.ec2.ami_|-test_ami_|-test_ami_|-search"]
        PRESENT_STATE += IMAGE_ANY
        ABSENT_STATE += IMAGE_ANY
    assert image_ret["result"], image_ret["comment"]


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="instance_type")
def test_instance_type(acct_data, __test):
    """
    Verify that we can get an image to do the rest of the tests
    """
    # Try to get the image created for this test by docker
    ret = run_yaml_block(INSTANCE_TYPE, acct_data=acct_data, test=__test)
    instance_type_ret = ret["aws.ec2.instance_type_|-nano_|-nano_|-search"]
    assert instance_type_ret["result"], instance_type_ret["comment"]
    assert instance_type_ret["new_state"], instance_type_ret["comment"]


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
def test_starting_absent(esm_cache, acct_data, __test):
    """
    Verify that the absent state is successful for a resource that never existed
    """
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret["aws.ec2.instance_|-test_resource_|-test_resource_|-absent"]
    assert instance_ret["result"], instance_ret["comment"]

    # Verify that the search state is not able to find the resource
    search_ret = ret["aws.ec2.instance_|-verify_|-verify_|-search"]
    assert not search_ret["result"], search_ret["comment"]

    assert instance_ret["new_state"] is None
    assert "aws.ec2.instance_|-test_resource_|-test_resource_|-" not in esm_cache


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present", depends=["image", "instance_type"])
def test_present(esm_cache, acct_data, __test):
    """
    Create a brand new instance based on an image that exists locally
    """
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        PRESENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret["aws.ec2.instance_|-test_resource_|-test_resource_|-present"]
    assert instance_ret["result"], instance_ret["comment"]


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present"])
def test_already_present(esm_cache, acct_data, __test):
    """
    The instance should exist in the cache, verify that the resource id gets populated and no changes are made
    """
    # Verify that the cache exists now
    assert (
        esm_cache
    ), "Cache does not exist! It should exist now after the previous test"

    # Run the state defined in the yaml block
    ret = run_yaml_block(
        PRESENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret["aws.ec2.instance_|-test_resource_|-test_resource_|-present"]
    assert instance_ret["result"], instance_ret["comment"]

    if not __test:
        assert not instance_ret["changes"], instance_ret["changes"]


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present"])
def test_describe(esm_cache, acct_data, __test):
    """
    Describe all instances and run the "present" state the described instance created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    tag = "aws.ec2.instance_|-test_resource_|-test_resource_|-"
    if tag not in esm_cache:
        raise pytest.skip(f"Instance is not yet searchable")

    # Get the instance_id of the instance created by this module
    instance_id = esm_cache[tag]["resource_id"]

    # Describe all instances
    ret = idem_cli("describe", "aws.ec2.instance", acct_data=acct_data)

    # Get the state for our new instance, that was created by describe
    single_described_state = yaml.safe_dump({"described": ret[instance_id]})

    # Run the present state for our resource created by describe
    ret = run_yaml_block(
        single_described_state,
        managed_state=esm_cache,
        acct_data=acct_data,
        test=__test,
    )
    tag = f"aws.ec2.instance_|-described_|-{instance_id}_|-present"
    assert tag in ret.keys()
    instance_ret = ret[tag]
    assert instance_ret["result"], instance_ret["comment"]

    # No changes should have been made!
    # We just created this state from describe
    assert not instance_ret["changes"], instance_ret["changes"]


# TODO Make a change, verify changes happen, each tool.aws.ec2.instance.update function should be tested independently


# Cleanup time


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present"])
def test_absent(esm_cache, acct_data, __test):
    """
    Terminate the instance
    """
    assert (
        esm_cache
    ), "Cache does not exist! It should exist now after the previous test"

    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret["aws.ec2.instance_|-test_resource_|-test_resource_|-absent"]
    assert instance_ret["result"], instance_ret["comment"]

    # Verify that the search state is not able to find the resource
    search_ret = ret["aws.ec2.instance_|-verify_|-verify_|-search"]
    assert not search_ret["result"], search_ret["comment"]

    assert instance_ret["new_state"] is None


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
def test_already_absent(esm_cache, acct_data, __test):
    """
    Verify that termination is idempotent
    """
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret["aws.ec2.instance_|-test_resource_|-test_resource_|-absent"]
    assert instance_ret["result"], instance_ret["comment"]

    # Verify that the search state is not able to find the resource
    search_ret = ret["aws.ec2.instance_|-verify_|-verify_|-search"]
    assert not search_ret["result"], search_ret["comment"]

    assert instance_ret["new_state"] is None
    assert "aws.ec2.instance_|-test_resource_|-test_resource_|-" not in esm_cache
